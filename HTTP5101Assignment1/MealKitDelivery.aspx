﻿<%@ Page Language="C#" %>
<!DOCTYPE html>
<html>
<head runat="server">
	<title>MealKitDelivery</title>
	<script runat="server">
	
	</script>
</head>
<body>
        <p> Welcome to your new favourite weeknight dinner! </br> 
            Fill out the form below to have all the ingridents and instructions
            for a fresh, tasty and convenient meal delivered right to your door! </p>
	<form id="form1" runat="server">
            <div>
            <p> What kinds of things do you look for in a meal? Check all that apply </p>
            <asp:CheckBox runat="server" ID="preference1" Text="Easy" />
            <asp:CheckBox runat="server" ID="preference2" Text="Hearty" />
            <asp:CheckBox runat="server" ID="preference3" Text="Healthy"/>
            <asp:CheckBox runat="server" id="preference4" Text="Light"/>
            <asp:CheckBox runat="server" id="preference5" Text="Impressive"/>
            </div>
            <div>
            <p> Dietary Preferences</p>
            <asp:RadioButton runat="server" Text="Carnivore" GroupName="diet"/>
            <asp:RadioButton runat="server" Text="Pescetarian" GroupName="diet"/>
            <asp:RadioButton runat="server" Text="Vegetarian" GroupName="diet"/>
            </div>
            <br />
            <asp:TextBox runat="server" id="allergies" placeholder="Any Allergies?"></asp:TextBox>
            <div>
            <p> How many people will your kit be feeding? (Please note max amount is 4)</p>
            <asp:TextBox runat="server" id="people" placeholder="# of people"></asp:TextBox>
            <asp:RangeValidator runat="server" ControlToValidate="people" Type="Integer" MinimumValue="1" MaximumValue="4" ErrorMessage="Please enter a number between 1 and 4"></asp:RangeValidator>
            </div>
            <div>
            <p> Delivery Day Preference </p>
            <asp:DropDownList runat="server" ID="deliverydate">
                <asp:ListItem Value="M" Text="Monday"></asp:ListItem>
                <asp:ListItem Value="T" Text="Tuesday"></asp:ListItem>
                <asp:ListItem Value="W" Text="Wednesday"></asp:ListItem>
                <asp:ListItem Value="T" Text="Thursday"></asp:ListItem>
                <asp:ListItem Value="F" Text="Friday"></asp:ListItem>
            </asp:DropDownList>
            </div>  
            <div>
            <p> Contact Information</p>
            <p> Full Name <asp:TextBox runat="server" id="name"></asp:TextBox> </p>
            <asp:RequiredFieldValidator runat="server" ControlToValidate="name" ErrorMessage="Please enter your full name"></asp:RequiredFieldValidator>
            <p> Address <asp:TextBox runat="server" id="address"></asp:TextBox> </p>
            <asp:RequiredFieldValidator runat="server" ControlToValidate="address" ErrorMessage="Please enter your address"></asp:RequiredFieldValidator>
            <p> Email <asp:TextBox runat="server" id="email"></asp:TextBox> </p>
            <asp:RegularExpressionValidator ID="emailvaildate" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="email" ErrorMessage="Please enter a valid email"></asp:RegularExpressionValidator>
           </div>
           </ br>
            <asp:Button id="Button1" Text="Sign Me Up" runat="server" />
            <asp:ValidationSummary id="valSum" DisplayMode="BulletList" runat="server" />
            

	</form>
</body>
</html>
